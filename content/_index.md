---
title: "Plot 82"
---
Welcome to Plot82.com, my online [journal](journal) where I record what occurs on my allotment plot. 
 
Plot 82 is quite long, so I could not fit it all in one photo, so here it is from around the middle, first looking North and then South
  
  ![View looking North](/photos/North1.jpg)  ![View looking South](/photos/South1.jpg)
